<?php
include_once('config.php');

if(!isset($_SESSION)){
 session_start();
}
IF (!isset($_SESSION['Username']))
  	{ header('Location: '. $homepage); }
	
$con = mysql_connect($SQLhost, $SQLuser, $SQLpass) or die('Could not connect: ' . mysql_error());
mysql_select_db($SQLdb) or die('Could not find database: ' . mysql_error());

$prof = mysql_fetch_assoc ( mysql_query ("SELECT Signature, Avatar FROM users WHERE Username = '".$_SESSION['Username']."' ") );

IF( isset($_POST['controle']) AND $_POST['controle']==TRUE ) {

IF ($_POST['bijwerken']=="signature") {
	IF ($_POST['signature'] == "")
		{ $error_signature = "Error! You haven't filled in your <u>Signature</u>!"; }
		ELSE
		{$sql_ber = (" UPDATE users SET Signature = '".mysql_real_escape_string($_POST['signature'])."' WHERE Username = '".$_SESSION['Username']."' "); 
		IF (!mysql_query ($sql_ber, $con) )
			{ die ("Query Error: " . mysql_error()); }
		header("Location: editprofile.php");
		}	
	}
ELSE IF ($_POST['bijwerken']=="password"){
	IF ( ($_POST['pass1'] == "") OR ($_POST['pass2'] == "") OR ($_POST['pass3'] == "") ) 
		{ IF ($_POST['pass1'] == "") {$error_pass1="You haven't entered this field!"; }
		  IF ($_POST['pass2'] == "") {$error_pass2="You haven't entered this field!"; }
		  IF ($_POST['pass3'] == "") {$error_pass3="You haven't entered this field!"; }
		}
	ELSE IF ($_POST['pass1'] != $_POST['pass2']) {
		$error_pass1 = "You haven't entered the same password twice!";
		$error_pass2 = "You haven't entered the same password twice!";
	}
	ELSE {
	$sql_oldpw = mysql_fetch_assoc ( mysql_query ("SELECT Password, salt FROM users WHERE Username = '".$_SESSION['Username']."' ") );
	IF ( md5(mysql_real_escape_string($_POST['pass3']) ).$sql_oldpw['salt'] != $sql_oldpw['Password'] )
		{ $error_pass3 = "This is not your current password!<br>Do you need help? Contact the admin <a href='mailto: ".$mailto." Password help'>here</a>!"; }
	ELSE { // hier gaat het eindelijk goed
		$sql_resetpw = (" UPDATE users SET Password = '".md5($_POST['pass2'])."' WHERE Username = '".$_SESSION['username']."' ");
		mysql_query ($sql_resetpw, $con) or die ("Query Error: ". mysql_error());
		header("Location: profile.php");
		}	
	}
}

mysql_close($con);
}
?>

<html>
<head>
<title>Edit Profile</title>
<link rel="icon" type="image/ico" href="/favicon.ico"> </link>
</head>
<body bgcolor="#CAFFAF">

<?php include('menu.php'); ?>

<table> <!-- Bericht toevoegen/bijwerken -->
<form action="" method="post">
<input type="hidden" name="bijwerken" value="signature">
<input type="hidden" name="controle" value="TRUE">
<tr> <td width=250>What is your Signature?</td>
<td width=100>
<TEXTAREA name="signature" rows="3" cols="40">
<?php if($prof['Signature'] != "" ){echo $prof['Signature']; } else { echo "Type here your signature!"; } ?>
</TEXTAREA></td>
<td width=10></td> <td width=600><font color="#FF0000"> <?php if(isset($error_signature)){echo $error_signature;} ?> </font> </td></tr>
<tr><td></td><td><input type="submit" value="Edit Signature"></td></tr>
</form> </table>
<hr>

<table> <!-- Avatar uploaden -->
<form action="upload_file.php" method="post" enctype="multipart/form-data">
<tr><td width=250><label for="file">Profile Picture:</label></td><td><input type="file" name="file" id="file"><br /> </td></tr>
<tr> <td></td> <td><b>Important:</b> the picture may only be a '.jpg' or '.jpeg' or '.png' or '.gif' !!</td></tr>
<tr> <td></td> <td><b>Important:</b> the picture must be smaller than 50 kb !!</td></tr>
<tr> <td></td> <td><b>Important:</b> the picture may not be larger than 150x150 pixels!</td></tr>
<tr><td></td> <td><input type="submit" name="submit" value="Upload Picture"></td></tr>
</form></table>

<hr>
<table> <!-- password veranderen -->
<form action="" method="post">
<input type="hidden" name="bijwerken" value="password">
<input type="hidden" name="controle" value="TRUE">
<tr><td width=250>Change your password:</td>
<td><input type="password" name="pass1" value="<?php if(isset($_POST['pass1'])){echo $_POST['pass1'];} ?>"></td> <td width=10></td>
<td width=600><font color="#FF0000"> <?php if(isset($error_pass1)){echo $error_pass1;} ?> </font> </td></tr>
<tr><td>Enter it again:</td>
<td><input type="password" name="pass2" value="<?php if(isset($_POST['pass2'])){echo $_POST['pass2'];} ?>"></td> <td width=10></td>
<td width=600><font color="#FF0000"> <?php if(isset($error_pass2)){echo $error_pass2;} ?> </font> </td></tr>
<tr><td>Enter your old password:</td>
<td><input type="password" name="pass3" value="<?php if(isset($_POST['pass3'])){echo $_POST['pass3'];} ?>"></td> <td width=10></td>
<td width=600><font color="#FF0000"> <?php if(isset($error_pass3)){echo $error_pass3;} ?> </font> </td></tr>
<tr><td></td> <td><input type="submit" value="Change Password"> </td> </tr>
</form></table>

<hr>
<form action="users.php" method="post">
<input type="hidden" name="profiel" value="">
<input type="submit" value="Back to Profile"> </form>
</body>
</html>