<?php
if(!isset($_SESSION)){ session_start(); }

$homepage = 'http://localhost/forum/';
$sitename = '! Awesome Forum ! ';
$mailto = 'myemail';

$buttonwidth=70;
$spoilercolor = 'AFC9FF';

$SQLhost = 'localhost';
$SQLuser = 'root';
$SQLpass = '';
$SQLdb = 'forum';

function mkdate($date) {
	$date = date('d-m-Y', strtotime($date));
return $date; }

function mkdatetime($datetime) {
	$datetime = date('d-m-Y H:i:s', strtotime($datetime));
return $datetime; }

function bbcode($msg) { #16*2=32
	$bb = array('[b]', '<b>', '[/b]', '</b>', '[i]', '<i>', '[/i]', '</i>', '[u]', '<u>', '[/u]', '</u>', '[enter]', '<br>', '[color=', '<font color="#',
				'[/color]', '</font>', '[size=', '<font size="', '[/size]', '</font>', '[url=', '<a href="', '[/url]', '</a>', '[spoiler]', 
				'<div class="spoiler"><input type="button" onclick="showSpoiler(this);" value="Show / Hide" /> <div class="inner" style="display:none;">',
				'[/spoiler]', '</div></div>', ']', '">' );
	for ($i=0; $i<32; $i+=2) {
		$msg = str_replace($bb[$i], $bb[$i+1], $msg);
	} 
	return $msg;
}
#how to do [code] examples[/code]?
# < = &lt || > = &gt
#what about the rest?

function calcAge(int $dag, int $maand, int $jaar) {
	$gebdatum = mktime(0, 0, 0, $maand, $dag, $jaar);
	$t = time();
	$lsec = ($gebdatum < 0) ? ( $t + ($gebdatum * -1) ) : $t - $gebdatum;
	$leeftijd = floor($lsec / "31536000");
	return $leeftijd;
}

function calcAge2(String $birthday){
	$birthDate = explode("-", $birthDate);
	$age1 = calcAge($birthDate[1], $birthDate[0], $birthDate[2]);
	return $age1;
}

function generateRandomString(int $length) {
	$chars = '0123456789abcdefghijklmnopqrstuvwxyz';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $chars[rand(0, strlen($chars) - 1)]; 
	}
	return $randomString; 
}
?>